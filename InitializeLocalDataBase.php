<?php

/**
 * Created by PhpStorm.
 * User: ����
 * Date: 20.03.2019
 * Time: 22:53
 */

class InitializeLocalDataBase
{

    public $dataConnection = [
        'host'      => '127.0.0.1',
        'username'     => '',
        'password'  => '',
        'dbname'    => 'stozap',
        'port'      => '3306',
        'charset'   => 'utf8'
    ];

    private $instance = null;

    function __construct($db_name)
    {

        if(!empty($db_name)){

            $this->dataConnection['dbname'] = $db_name;

        }

        ini_set("memory_limit", -1);

        $dsn = sprintf('mysql:host=%s:%s;dbname=%s;charset=%s',
            $this->dataConnection['host'],
            $this->dataConnection['port'],
            $this->dataConnection['dbname'],
            $this->dataConnection['charset']
        );
        try {

            $this->instance = new PDO($dsn, $this->dataConnection['username'], $this->dataConnection['password']);

        } catch (PDOException $e) {

            echo "error!: " . $e->getMessage() . "<br>";
            echo "\n"."�������� ����� ���� ������ �� ���������� ��� ������ ��������� ����! ���������� � ������ ���������!";
            die();

        }

    }

    public function getInstance(){

        if($this->instance != null){

            return $this->instance;

        }

    }

}