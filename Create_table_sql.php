<?php
/**
 * Created by PhpStorm.
 * User: Илия
 * Date: 11.12.2018
 * Time: 19:26
 */
class Create_table_sql{

    public static $_instance = null;

    function __construct()
    {

    }
#Инстанс Базы Данных
     function connection_database($db_local, $dsn){

        try {
            $pdo = new PDO($dsn, $db_local['username'], $db_local['password']);

        } catch (PDOException $e) {

            echo "error!: " . $e->getMessage() . "<br>";
            echo "\n"."Возможно такая база данных не существует или указан ошибочный порт! Обратитесь в службу поддержки!";
            die();

        }
        return $pdo;
    }
#Проверка на наличие таблицы в Базе Данных
    function check_table_on_BD($instance_db, $name_table){

        $query_check_the_table = "SHOW TABLES LIKE '$name_table';";
        $query_object_check_the_table = $instance_db->prepare($query_check_the_table);
        $query_object_check_the_table->execute();

        $exist_table = $query_object_check_the_table->fetchColumn();

        return $exist_table;

    }
#Создание таблица в базе данных
    function create_table($instance_db, $name_table, array $name_type_column )
    {

        $exist_table = $this->check_table_on_BD($instance_db, $name_table);

        if ($exist_table) {

            $name_data_base = $instance_db->query('SELECT DATABASE()')->fetchColumn();
            echo "Таблица - '$name_table' уже существует в базе данных - '$name_data_base'";

        } else {

            $query_create_table = "CREATE TABLE `$name_table` (
                              id int(11) NOT NULL AUTO_INCREMENT,";

            foreach ($name_type_column as $name => $type) {

                $query_create_table .= "`$name` $type DEFAULT NULL,";

            }

            $query_create_table .= "PRIMARY KEY (id) ) ENGINE = INNODB;";

            $query_object_create_table = $instance_db->prepare($query_create_table);

            if($query_object_create_table->execute()){

                echo "Таблица $name_table успешно создана!";

            } else {

                $error_array = $query_object_create_table->errorInfo();

                 echo "Код ошибки SQLSTATE: ". $error_array[0]."\n";
                 echo "Код ошибки, заданный драйвером: ". $error_array[1]."\n";
                 echo "Сообщение об ошибке, заданное драйвером: ". $error_array[2];
            }

        }
    }


}

include 'data_bd_connection.php';

$newObject = new Create_table_sql();

$newObject->create_table($newObject->connection_database($db_local,$dsn), "table1", array("article" => "varchar(255)", "proiz" => "varchar(255)"));
